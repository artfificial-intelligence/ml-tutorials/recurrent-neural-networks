# 시퀀스 데이터를 위한 순환 신경망 <sup>[1](#footnote_1)</sup>

> <font size="3">시퀀스 데이터 분석을 위해 순환 신경망을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./recurrent-neural-networks.md#intro)
1. [순환 신경망이란?](./recurrent-neural-networks.md#sec_02)
1. [순환 신경망이 시퀀스 데이터에 유용한 이유?](./recurrent-neural-networks.md#sec_03)
1. [순환 신경망의 종류](./recurrent-neural-networks.md#sec_04)
1. [순환 신경망 훈련법](./recurrent-neural-networks.md#sec_05)
1. [순환 신경망의 응용](./recurrent-neural-networks.md#sec_06)
1. [순환 신경망의 문제점과 한계](./recurrent-neural-networks.md#sec_07)
1. [마치며](./recurrent-neural-networks.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 10 — Recurrent Neural Networks for Sequence Data](https://medium.datadriveninvestor.com/ml-tutorial-10-recurrent-neural-networks-for-sequence-data-72adf8f30654?sk=68d9590230299261c46f86c8e9af7427)를 편역하였다.